require 'net/http'
require 'benchmark'
require 'argv-parse/argv-parse'

# The main SiteStatus class
class SiteStatus

	# Check the status of a site and return the amount of time it takes for the site to respond
	#
	# Example:
	# >> SiteStatus.check('https://about.gitlab.com')
	# => 0.5459379999665543
	#
	# Arguments:
	#   site: (String)
	#
	# Return:
	# 	time (in seconds): (Float)
	#

	def self.check(site)
		uri = URI(site)

		time = Benchmark.realtime { Net::HTTP.get(uri) }

		time
	end

	# Take the average response time for a site over a specific amount of time in seconds and with a number of intervals
	#
	# Example:
	# -- Just site, defailts to 60 seconds with 10 second intervals
	# >> SiteStatus.average('https://about.gitlab.com')
	#	=> 0.45926866662921384
	#
	#	-- Site with just only seconds specified defaults to 10 second intervals
	#	>> SiteStatus.average('https://about.gitlab.com', 30)
	#	=> 0.45335966666849953
	#
	#	-- Site with seconds and intervals
	#	>> SiteStatus.average('https://about.gitlab.com', 30, 5)
	#	=> 0.43411833329203847
	#
	#	Arguments:
	#		site: (String)
	#		seconds: (Integer)
	#		intervals: (Integer)
	#
	#	Return:
	#		time (in seconds): (Float)
	#
	
	def self.average(site, seconds = 60, intervals = 10)
		realtimes = []
		num_iterations = seconds / intervals

		num_iterations.times do
			realtimes << check(site)
			sleep intervals
		end

		realtimes.reduce(:+) / realtimes.size.to_f

	end
end
