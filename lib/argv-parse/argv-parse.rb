# ArgvParse class, for parsing commannd line arguments.

class ArgvParse

	# Parse the Argv input from the command line specific to the site-status CLI.
	# Example: 
	# >> ArgvParse.parse(ARGV)
	# => {site: 'https://about.gitlab.com', seconds: 60, intervals: 10}
	#
	# Arguments:
	# 	argv: (Array:String)
	#	
	# Return:
	# 	args: (Object: site:String, seconds:Integer, interval: Integer)
	
	def self.parse(argv)
		args = {}
		argv.each do |a|
			arg = a.split('=')
			next if arg.length < 2 || arg.length > 2

			case arg.first
			when "--site"
				args[:site] = arg.last
			when "--seconds"
				args[:seconds] = arg.last.to_i
			when "--intervals"
				args[:intervals] = arg.last.to_i
			else
				nil
			end
		end
		args
	end
end
