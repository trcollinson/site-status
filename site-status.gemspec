Gem::Specification.new do |s|
	s.name = 'site-status'
	s.version = '0.0.2'
	s.date = '2017-11-21'
	s.summary = 'A gem for checking site status'
	s.description = 'A gem for checking the status of a site. Examples include https://gitlab.com and https://about.gitlab.com'
	s.authors = ['Tim Collinson']
	s.email = 'trcollinson@gmail.com'
	s.files = [
		'lib/site-status.rb',
		'lib/argv-parse/argv-parse.rb'
	]
	s.executables = ['site-status']
	s.homepage = 'https://gitlab.com/trcollinson/site-status'
	s.license = 'MIT'
	s.add_development_dependency 'rspec', '~> 3.7'
	s.add_development_dependency 'webmock', '~> 3.1'
end

