require 'spec_helper'

describe SiteStatus do
	before do
		stub_request(:any, "www.example.com")
		allow(Benchmark).to receive(:realtime).and_return(0.1)
		allow(Object).to receive(:sleep)
	end

	it 'should return a number of miliseconds for a site check' do
		expect(described_class.check('http://www.example.com')).to eq(0.1)
	end

	it 'should return an average over 60 seconds if no seconds or intervals are specified' do
		expect(described_class.average('http://www.example.com')).to be_within(0.01).of(0.1)	
	end

	it 'should return an average over any number of seconds if no interval is specified' do
		expect(described_class.average('http://www.example.com', 30)).to be_within(0.01).of(0.1)
	end

	it 'should return an average over any number of seconds and any interval length' do
		expect(described_class.average('http://www.example.com', 30, 5)).to be_within(0.01).of(0.1)
	end
end

