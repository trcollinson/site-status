require 'spec_helper'

describe ArgvParse do
	it 'should return empty if no argv is give' do
		expect(described_class.parse([])).to be_empty
	end
	it 'should parse a --site argument and return a site symbol' do
		expect(described_class.parse(['--site=http://www.example.com'])).to eq({site: 'http://www.example.com'})
	end
	it 'should parse a --seconds argument and return a seconds symbol' do
		expect(described_class.parse(['--seconds=60'])).to eq({seconds: 60})
	end
	it 'should parse a --intervals argument and return a intervals symbol' do
		expect(described_class.parse(['--intervals=10'])).to eq({intervals: 10})
	end
	it 'should ignore other arguments' do
		expect(described_class.parse(['--seconds=60', '--bad-arg=something'])).to eq({seconds: 60})
	end
	it 'should ignore empty arguments' do
		expect(described_class.parse(['--bad-arg', 'string', '--seconds=45'])).to eq({seconds: 45})
	end
end
