Site Status
===========

A Gem to check the status of any site by catching the amount of time a response takes.

Build and Install
-----------------

You can clone and build the gem on your local machine using the following method:

```
git clone git@gitlab.com:trcollinson/site-status.git

cd site-status

gem build site-status.gemspec

gem install site-status-0.0.1.gem
```

This will clone the repository into the `site-status`. From there you will build the gem and install it into your current ruby environment.

If you would prefer, you can add the following command to a Gemfile and then `bundle` to install the gem for testing.

```
gem 'site-status', :git => 'https://gitlab.com/trcollinson/site-status.git'
```

Usage
-----

You may use the Gem from the command line by running the `site-status` command like so:

```
site-status --site=https://about.gitlab.com --seconds=60 --intervals=10
```

The only required flag is `--site`. The other flags have sane defaults.

You may also try the gem out using `irb`. Try it like so:

```
require 'site-status'

# Take the average response time for a site over 60 seconds with a 10 second interval:
SiteStatus.average('https://about.gitlab.com')

# Take the average response time for a site over 30 seconds with a 10 second interval:
SiteStatus.average('https://about.gitlab.com', 30)

# Take the average response time for a site over 30 seconds with a 5 second interval:
SiteStatus.average('https://about.gitlab.com', 30, 5)
```

You may also get an individual response time status for any site like so:

```
require 'site-status'

SiteStatus.check('https://about.gitlab.com')
```

Contributing
------------

### Bugs, Issues, and Feature Requests

If you find a bug, issue, or have a feature request, please create a new issue here: https://gitlab.com/trcollinson/site-status/issues

### Developing

In order to work on Site Status, please fork from this repository and clone. Do any work on a dedicated branch and rebase against the master branch of this repository before submitting a Merge Request.

Make sure to add tests in the appropriate spec file and run specs using the `rspec` command.

License
-------

The gem is available as open source under the terms of the MIT License.

